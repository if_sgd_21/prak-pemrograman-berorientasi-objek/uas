# Pemrograman Berorientasi Objek

## Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
**USE CASE USER**

| Nama Use Case | Prioritas | Status |
| ------ | ------ | ------ |
| Pengguna dapat mengakses aplikasi secara online | Tinggi | Selesai |
| Pendaftaran Pengguna | Tinggi | Selesai |
| Pendaftaran Pengguna Premium | Tinggi | Belum Dimulai |
| Pengguna bisa masuk/login | Tinggi | Selesai |
| Mengunggah tweet | Tinggi | Selesai |
| Menghapus tweet | Tinggi | Selesai |
| Mengubah tweet | Tinggi | Belum Dimulai |
| Mengomentari tweet | Tinggi | Belum Dimulai |
| Membagikan tweet (retweet) | Tinggi | Belum Dimulai |
| Menyimpan tweet (Bookmark) | Tinggi | Belum Dimulai |
| Menampilkan Nama Pengguna | Tinggi | Selesai |
| Menampilkan verified user(centang biru) | Tinggi | Selesai |
| Menampilkan tweet | Tinggi | Selesai |
| Menampilkan followers/following | Tinggi | Belum Dimulai |
| Pengguna dapat menyukai tweet | Tinggi | Belum Dimulai |
| Pengguna dapat mengikuti Pengguna lain | Tinggi | Belum Dimulai |
| Pengguna dapat diikuti oleh Pengguna lain | Tinggi | Belum Dimulai |
| Menyaring konten | Tinggi | Belum Dimulai |
| Mencari Pengguna lain| Sedang | Belum Dimulai |
| Mengubah Nama Pengguna | Sedang | Belum Dimulai |
| Mengubah password | Sedang | Selesai |
| Mengubah email | Sedang | Selesai |
| Mengubah nama (nama depan & belakang) | Sedang | Belum Selesai |
| Mengubah bio | Sedang | Selesai |
| Menampilkan App Version | Rendah | Belum Dimulai |
| Menampilkan profile (name, Nama Pengguna, bio, etc.) | Rendah | Selesai |

**Use case manajemen perusahaan**
| Nama Use Case | Prioritas | Status |
| ------ | ------ | ------ |
| Memantau dan mengelola konten yang diposting oleh pengguna, termasuk tweet, gambar, dan video.|Tinggi|Belum Dimulai|
| Mengelola komunikasi dengan pengguna, media, atau publik terkait dengan masalah kontroversial atau peristiwa penting.|Tinggi|Belum Dimulai|
|Melakukan moderasi konten yang melanggar kebijakan atau berpotensi merugikan.|Tinggi|Belum Dimulai|
|Menghapus konten yang tidak sesuai atau merusak reputasi perusahaan.|Tinggi|Belum Dimulai|
|Menyusun dan memperbarui kebijakan penggunaan dan pedoman komunitas Tweetify.|Tinggi|Belum Dimulai|
|Mengkomunikasikan kebijakan kepada pengguna dan menjelaskan pelanggaran yang dilakukan.|Tinggi|Belum Dimulai|
|Mengambil tindakan hukuman atau peringatan terhadap pengguna yang melanggar kebijakan.|Tinggi|Belum Dimulai|
|Melakukan pemantauan terhadap aktivitas mencurigakan atau serangan keamanan.|Tinggi|Belum Dimulai|
|Menghapus akun pengguna yang tidak aktif atau melanggar kebijakan.|Tinggi|Belum Dimulai|
|Mengelola tim yang terlibat dalam operasional dan pengembangan platform Tweetify.|Tinggi|Belum Dimulai|
|Menyampaikan informasi penting kepada pengguna tentang pembaruan, perbaikan bug, atau fitur baru.|Tinggi|Belum Dimulai|
|Menyediakan dukungan dan bantuan kepada pengguna yang mengalami masalah teknis atau kehilangan akses ke akun.|Tinggi|Belum Dimulai|
|Memastikan keamanan sistem dan data pengguna.|Tinggi|Belum Dimulai|

**Use case direksi perusahaan (dashboard, monitoring, analisis)**
| Nama Use Case | Prioritas | Status |
| ------ | ------ | ------ |
|Melihat Pertumbuhan Pengguna (perbulan)|Tinggi|Belum Dimulai|
|Mengelompokkan Pengguna Berdasarkan Negara|Tinggi|Belum Dimulai|
|Mengelompokkan Pengguna Berdasarkan Jenis Akun |Tinggi|Belum Dimulai|
|Menganalisis Statistik Langganan Tweetify |Tinggi|Belum Dimulai|
|Mengatur Pelanggaran Tweet|Tinggi|Belum Dimulai|
|Mengelompokkan Tweet Berdasarkan Jumlah Likes |Sedang|Belum Dimulai|
|Menganalisis data dan tren penggunaan untuk mendapatkan wawasan bisnis.|Tinggi|Belum Dimulai|
|Mengidentifikasi tren dan pola perilaku pengguna untuk pengambilan keputusan strategis.|Tinggi|Belum Dimulai|

## Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
![Class Diagram Sementara](assets/class-diagram-tweetify.png)
## Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
Single Responsibility Principle (SRP):
```php
<?php
class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $name = DB_NAME;
    private $dbh;
    private $stmt;

    // constructor database
    public function __construct()
    {
        // data source name
        $dsn = 'mysql:host='. $this->host . ';dbname='. $this->name;

        // option
        $option = [
                // persistent connection
                // untuk menghindari koneksi ke database terus menerus
            PDO::ATTR_PERSISTENT => true,
                // error mode
                // untuk menampilkan error
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];
        // ... Kode lainnya tidak saya masukkan karena kepanjangan ...
```
Kelas Database bertanggung jawab hanya untuk mengelola koneksi dan operasi database.
Kelas ini memiliki satu tanggung jawab yaitu mengatur koneksi ke database dan menjalankan query.


## Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
**Design Pattern yang dipilih: MVC (Model, View, Controller)**

![MVC Tweetify](assets/MVC_Tweetify.png)

**MVC** adalah singkatan dari Model-View-Controller, yang merupakan paradigma desain yang digunakan dalam pengembangan perangkat lunak untuk memisahkan logika bisnis, representasi tampilan, dan interaksi pengguna. Berikut adalah penjelasan tentang setiap komponen dalam MVC:

**Model:**
Model mewakili data dan aturan bisnis dalam aplikasi. Ini bertanggung jawab untuk memanipulasi dan mengelola data serta mengimplementasikan logika bisnis. Model biasanya berisi kelas-kelas yang merepresentasikan objek-objek dalam sistem, seperti entitas, fungsi, atau objek yang terkait dengan basis data. Model juga mengatur akses ke data dan memberikan metode untuk melakukan operasi CRUD (Create, Read, Update, Delete) pada data.

**View:**
View bertanggung jawab untuk menampilkan informasi kepada pengguna dan mengatur tampilan dari data yang dihasilkan oleh model. Tampilan dapat berupa antarmuka pengguna (UI) atau output yang dihasilkan dalam format lain seperti HTML, XML, atau JSON. View tidak berisi logika bisnis, tetapi hanya berfokus pada tampilan visual dari data yang diberikan oleh model.

**Controller:**
Controller bertindak sebagai perantara antara model dan view. Ini menangani input pengguna, menginterpretasikan permintaan, dan memperbarui model atau tampilan sesuai dengan input yang diterima. Controller juga dapat memvalidasi data sebelum mengirimkannya ke model dan mengambil tindakan yang sesuai berdasarkan interaksi pengguna. Dalam banyak kerangka kerja MVC, controller menerima permintaan dari routing sistem dan memilih model dan view yang tepat untuk merespons permintaan tersebut.
## Mampu menunjukkan dan menjelaskan konektivitas ke database

```php
<?php
class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $name = DB_NAME;
    private $dbh;
    private $stmt;

    // constructor database
    public function __construct()
    {
        // data source name
        $dsn = 'mysql:host='. $this->host . ';dbname='. $this->name;

        // option
        $option = [
                // persistent connection
                // untuk menghindari koneksi ke database terus menerus
            PDO::ATTR_PERSISTENT => true,
                // error mode
                // untuk menampilkan error
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        // check connection
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $option);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
?>
```
Pertama, kelas Database didefinisikan. Ini adalah kelas yang bertanggung jawab untuk mengatur dan menjaga koneksi ke database.
Dalam konstruktor kelas Database, variabel-variabel seperti host, pengguna, kata sandi, dan nama database diinisialisasi. Nilai-nilai ini diambil dari konstanta yang didefinisikan di file config.php pada folder app/config.

DSN (data source name) dibentuk dengan menggabungkan nilai host dan nama database yang telah ditentukan dalam format yang sesuai dengan driver PDO yang digunakan. Dalam contoh ini, kita menggunakan driver PDO untuk MySQL, sehingga DSN terdiri dari 'mysql:host=' diikuti oleh nilai host, kemudian 'dbname=' diikuti oleh nama database.

Selanjutnya, opsi PDO ditentukan. Dalam contoh ini, saya mengatur dua opsi:

PDO::ATTR_PERSISTENT: Ini mengaktifkan koneksi persisten. Koneksi persisten memungkinkan koneksi yang sama digunakan secara berulang, menghindari overhead yang terkait dengan membuka dan menutup koneksi database.

PDO::ATTR_ERRMODE: Ini mengatur mode kesalahan PDO ke PDO::ERRMODE_EXCEPTION. Dengan mengatur ini, PDO akan menghasilkan pengecualian (exception) ketika terjadi kesalahan, memungkinkan kita untuk menangkap dan menangani kesalahan tersebut.

Dalam blok try-catch, objek PDO dibuat dengan menerima DSN, nama pengguna, kata sandi, dan opsi yang telah ditentukan sebelumnya. Jika koneksi ke database berhasil, objek PDO akan diinisialisasi dan disimpan dalam variabel $dbh (database handle) yang merupakan properti dari kelas Database. Jika terjadi kesalahan dalam koneksi, pengecualian (exception) akan ditangkap dan pesan kesalahan akan ditampilkan menggunakan die().

Lalu bagaimana cara kita konek ke database saat kita hendak melakukan query seperti login, bikin tweet, dll?
```php
class LoginModel {
	
	private $table = 'user';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

    // ......Sebagian kode tidak ditampilkan......
```
Adalah dengan memanggil constructor dari kelas database tadi.
## Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
- Web service yang dipakai: **PHP Slim**
![Web Service Tweetify](assets/web-service-tweetify.png)

## Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
- Halaman Daftar

![Web Service Tweetify](assets/Sign-up.png)
- Halaman Login

![Web Service Tweetify](assets/Login.png)
- Halaman Beranda

![Web Service Tweetify](assets/Home.png)
## Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
```php
<?php
// URL yang akan diakses
$url = "https://api.example.com/data";

// Melakukan permintaan HTTP GET
$response = file_get_contents($url);

// Menampilkan respons HTTP
echo $response;
?>
```
## Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
Link Youtube: 
## BONUS !!! Mendemonstrasikan penggunaan Machine Learning
Link Youtube:
